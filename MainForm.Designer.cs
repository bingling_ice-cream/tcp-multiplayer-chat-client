﻿
namespace TCP多人聊天室客户端
{
    partial class MainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.label_Ip = new System.Windows.Forms.Label();
            this.textBox_Ip = new System.Windows.Forms.TextBox();
            this.textBox_Port = new System.Windows.Forms.TextBox();
            this.label_Port = new System.Windows.Forms.Label();
            this.button_Connect = new System.Windows.Forms.Button();
            this.button_Cut = new System.Windows.Forms.Button();
            this.label_Send = new System.Windows.Forms.Label();
            this.textBox_Message = new System.Windows.Forms.TextBox();
            this.button_Send = new System.Windows.Forms.Button();
            this.richTextBox_Message = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // label_Ip
            // 
            this.label_Ip.AutoSize = true;
            this.label_Ip.Location = new System.Drawing.Point(25, 23);
            this.label_Ip.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_Ip.Name = "label_Ip";
            this.label_Ip.Size = new System.Drawing.Size(72, 16);
            this.label_Ip.TabIndex = 0;
            this.label_Ip.Text = "服务端IP";
            // 
            // textBox_Ip
            // 
            this.textBox_Ip.Location = new System.Drawing.Point(115, 17);
            this.textBox_Ip.Margin = new System.Windows.Forms.Padding(4);
            this.textBox_Ip.Name = "textBox_Ip";
            this.textBox_Ip.Size = new System.Drawing.Size(233, 26);
            this.textBox_Ip.TabIndex = 1;
            // 
            // textBox_Port
            // 
            this.textBox_Port.Location = new System.Drawing.Point(115, 51);
            this.textBox_Port.Margin = new System.Windows.Forms.Padding(4);
            this.textBox_Port.Name = "textBox_Port";
            this.textBox_Port.Size = new System.Drawing.Size(233, 26);
            this.textBox_Port.TabIndex = 3;
            // 
            // label_Port
            // 
            this.label_Port.AutoSize = true;
            this.label_Port.Location = new System.Drawing.Point(25, 57);
            this.label_Port.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_Port.Name = "label_Port";
            this.label_Port.Size = new System.Drawing.Size(88, 16);
            this.label_Port.TabIndex = 2;
            this.label_Port.Text = "服务端端口";
            // 
            // button_Connect
            // 
            this.button_Connect.Location = new System.Drawing.Point(366, 20);
            this.button_Connect.Name = "button_Connect";
            this.button_Connect.Size = new System.Drawing.Size(75, 23);
            this.button_Connect.TabIndex = 4;
            this.button_Connect.Text = "连接";
            this.button_Connect.UseVisualStyleBackColor = true;
            this.button_Connect.Click += new System.EventHandler(this.Button_Connect_Click);
            // 
            // button_Cut
            // 
            this.button_Cut.Location = new System.Drawing.Point(366, 54);
            this.button_Cut.Name = "button_Cut";
            this.button_Cut.Size = new System.Drawing.Size(75, 23);
            this.button_Cut.TabIndex = 5;
            this.button_Cut.Text = "断开";
            this.button_Cut.UseVisualStyleBackColor = true;
            this.button_Cut.Click += new System.EventHandler(this.Button_Cut_Click);
            // 
            // label_Send
            // 
            this.label_Send.AutoSize = true;
            this.label_Send.Location = new System.Drawing.Point(25, 88);
            this.label_Send.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_Send.Name = "label_Send";
            this.label_Send.Size = new System.Drawing.Size(72, 16);
            this.label_Send.TabIndex = 6;
            this.label_Send.Text = "发送消息";
            // 
            // textBox_Message
            // 
            this.textBox_Message.Location = new System.Drawing.Point(115, 85);
            this.textBox_Message.Margin = new System.Windows.Forms.Padding(4);
            this.textBox_Message.Name = "textBox_Message";
            this.textBox_Message.Size = new System.Drawing.Size(233, 26);
            this.textBox_Message.TabIndex = 7;
            // 
            // button_Send
            // 
            this.button_Send.Location = new System.Drawing.Point(366, 86);
            this.button_Send.Name = "button_Send";
            this.button_Send.Size = new System.Drawing.Size(75, 23);
            this.button_Send.TabIndex = 8;
            this.button_Send.Text = "发送";
            this.button_Send.UseVisualStyleBackColor = true;
            this.button_Send.Click += new System.EventHandler(this.Button_Send_Click);
            // 
            // richTextBox_Message
            // 
            this.richTextBox_Message.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.richTextBox_Message.Location = new System.Drawing.Point(0, 127);
            this.richTextBox_Message.Name = "richTextBox_Message";
            this.richTextBox_Message.Size = new System.Drawing.Size(469, 473);
            this.richTextBox_Message.TabIndex = 9;
            this.richTextBox_Message.Text = "";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(469, 600);
            this.Controls.Add(this.richTextBox_Message);
            this.Controls.Add(this.button_Send);
            this.Controls.Add(this.textBox_Message);
            this.Controls.Add(this.label_Send);
            this.Controls.Add(this.button_Cut);
            this.Controls.Add(this.button_Connect);
            this.Controls.Add(this.textBox_Port);
            this.Controls.Add(this.label_Port);
            this.Controls.Add(this.textBox_Ip);
            this.Controls.Add(this.label_Ip);
            this.Font = new System.Drawing.Font("楷体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "MainForm";
            this.Text = "TCP多人聊天室客户端";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_Ip;
        private System.Windows.Forms.TextBox textBox_Ip;
        private System.Windows.Forms.TextBox textBox_Port;
        private System.Windows.Forms.Label label_Port;
        private System.Windows.Forms.Button button_Connect;
        private System.Windows.Forms.Button button_Cut;
        private System.Windows.Forms.Label label_Send;
        private System.Windows.Forms.TextBox textBox_Message;
        private System.Windows.Forms.Button button_Send;
        private System.Windows.Forms.RichTextBox richTextBox_Message;
    }
}

